package com.basics.algorithms;
/** To find if a input string has all unique characters*/
public class UniqueString {
	
	public static void main (String args[]) {
		String s = "abcdefa";
		if (checkUniqueString(s))
			System.out.println("unique String "+s);
		else
			System.out.println("String doesnt contain unique chars..");
	}
	
	private static boolean checkUniqueString(String input) {
		for (char c : input.toCharArray()) {
			if (input.indexOf(c) != input.lastIndexOf(c)) {
				System.out.println("Not UniqueString Found repeated char "+c);
				return false;
			}			
		}
		return true;
	}
}
