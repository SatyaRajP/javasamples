package com.basics.data_structures;

import java.nio.ByteBuffer;
import java.util.concurrent.ExecutionException;

import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.KinesisProducerConfiguration;
import com.amazonaws.services.kinesis.producer.UserRecordResult;
import com.google.common.util.concurrent.ListenableFuture;

public class AWSKinesisProducer {
	
	public static void main (String args[]) throws InterruptedException, ExecutionException {
		KinesisProducer kp = new KinesisProducer();
		String payload = "{HelloWorld:1}";
		ListenableFuture<UserRecordResult> future = kp.addUserRecord("sunnxt-kb-kinesis", "event-1", ByteBuffer.wrap(payload.getBytes()));
		System.out.println("Status: "+future.isDone()+ future.get());
	}

}
