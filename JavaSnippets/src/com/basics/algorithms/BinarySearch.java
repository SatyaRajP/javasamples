package com.basics.algorithms;

import java.util.Arrays;
/** To demonstrate binary search algorithm */
public class BinarySearch {
	
	public static void main(String args[]) {
		String[] stringArray = {"abc","bcd","cde","def","efg","abc"};
		String searchFor = "ab";
		System.out.println(String.format("Length of array is %d", stringArray.length));
		Arrays.sort(stringArray);
		System.out.println("Sorted array is: "+Arrays.toString(stringArray));
		System.out.println("Found at "+ binSearch(stringArray,searchFor));
	}
	
	public static int binSearch(String[] arr, String searchFor) {
		//Java has built in binarysearch method in Arrays class
		//System.out.println("built in: "+Arrays.binarySearch(arr, searchFor));
		return binarySearch(arr, 0,arr.length,searchFor);
	}

	private static int  binarySearch(String[] arr, int i, int length, String searchFor) {
			while (i <= length) {
				int mid = (i+length) /2;
				if (arr[mid].compareToIgnoreCase(searchFor)==0) {
					System.out.println(String.format("Search key found at %d in %s",mid,Arrays.toString(arr)));
					return mid;
				}else if(arr[mid].compareToIgnoreCase(searchFor) > 0){
					//comparetTo returns positive if searchFor less than mid
					length = mid-1;
				}else if(arr[mid].compareToIgnoreCase(searchFor)<0) {
					//compareTo returns negative if searchFor greater than mid
					i = mid+1;
				}
				//System.out.println(String.format("values are %d-%d-%d",mid,i,length));
			}	
			System.out.println(searchFor+" Not found");
			return -1;
	}
}
