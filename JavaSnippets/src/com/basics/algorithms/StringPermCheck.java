package com.basics.algorithms;

public class StringPermCheck {
	
	public static void main (String args[]) {
		String s1 = "cocoon";
		String s2 = "conoc";
		checkPermutation (s1,s2);
	}
	
	//Enable length condition based on requirement and question
	private static int checkPermutation(String s1, String s2) {
		//if ((s1.length() == s2.length())) {
			for (char c: s1.toCharArray()) {
				if (s2.indexOf(c) == -1) {
					System.out.println("Char not found. Not permutation."+c);
					return -1;
				}
			}
			System.out.println("Is permutation "+s1+"-"+s2);
			return 1;
	/*	}else {
			System.out.println("Not permutation "+s1+"-"+s2);
			return -1;
		}*/
	}

}
