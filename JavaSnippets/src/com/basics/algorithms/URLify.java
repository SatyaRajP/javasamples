package com.basics.algorithms;

public class URLify {

	public static void main (String args[]) {
		String s = "Mr John Smith    ";
		s = s.trim().replaceAll("\\s+", " ");
		System.out.println("After replacing multiple spaces with one: "+s+"|");
		System.out.println(s.replaceAll("\\s", "%20"));
	}
}
