package com.image.processing;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import magick.ImageInfo;
import magick.ImageMagick;
import magick.Magick;
import magick.MagickException;
import magick.MagickImage;

public class ImageProcess {

	public static void main(String[] args) throws IOException, MagickException {
		System.out.println(processImageNative());
		System.out.println(processImageJMagick());	
	}
	
	
	public static Boolean processImageNative() throws IOException {
		BufferedImage image = ImageIO.read(new File("E:/TestData/images/wfl_101021_3.jpg"));
		Image newImg = image.getScaledInstance(625, 1250, Image.SCALE_DEFAULT );
		BufferedImage bImg = new BufferedImage(625, 1250, Image.SCALE_DEFAULT);
		bImg.getGraphics().drawImage(newImg, 0, 0, null);
		ImageIO.write(bImg, "jpg", new File("E:/TestData/images/wfl_resize.jpg"));
		return true;
	}
	
	public static Boolean processImageJMagick() throws MagickException {
		ImageInfo info = new ImageInfo("E:/TestData/images/kkm_90050.jpg");
		MagickImage image = new MagickImage(info);
		image = image.scaleImage(625, 1250);
		image.setFileName("E:/TestData/images/kkm_resized.jpg");
		image.writeImage(info);
		return true;
	}

}
